CREATE TABLE group_role (
    id SERIAL NOT NULL,
    group_id INTEGER NOT NULL,
    role_id INTEGER NOT NULL,
    created TIMESTAMP DEFAULT now() NOT NULL,
    updated TIMESTAMP DEFAULT now() NOT NULL,
    CONSTRAINT group_role_id_pk PRIMARY KEY (id),
    CONSTRAINT group_role_group_id_fk FOREIGN KEY (group_id)
        REFERENCES groups(id),
    CONSTRAINT group_role_role_id_fk FOREIGN KEY (role_id)
        REFERENCES roles(id),
    CONSTRAINT group_role_group_id_role_id_uk UNIQUE (group_id, role_id)
);

COMMENT ON TABLE group_role IS 'Tabla pivote entre Groups y Roles con relación de muchos a muchos';

COMMENT ON COLUMN group_role.id IS 'Clave principal de la entidad Group_Role';
COMMENT ON COLUMN group_role.group_id IS 'Id del grupo (Groups)';
COMMENT ON COLUMN group_role.role_id IS 'Id del role (Roles)';
COMMENT ON COLUMN group_role.created IS 'Identifica la fecha de cuando se le asigno un role a un grupo';
COMMENT ON COLUMN group_role.updated IS 'Identifica la fecha de actualización del role de un grupo';