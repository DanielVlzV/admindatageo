CREATE TABLE resources(
    id SERIAL NOT NULL,
    resourcename VARCHAR(50) NOT NULL,
    resourcedesc VARCHAR(250) DEFAULT NULL,
    created TIMESTAMP DEFAULT now() NOT NULL,
    updated TIMESTAMP DEFAULT now() NOT NULL,
    CONSTRAINT resources_id_pk PRIMARY KEY (id),
    CONSTRAINT resources_resourcename_uk UNIQUE (resourcename)
);

COMMENT ON TABLE resources IS 'Identifica los Recursos del sistema (Tablas)';

COMMENT ON COLUMN resources.id IS 'Clave principal de la entidad Resources';
COMMENT ON COLUMN resources.resourcename IS 'Nombre del recurso';
COMMENT ON COLUMN resources.resourcedesc IS 'Descripción del recurso';
COMMENT ON COLUMN resources.created IS 'Identifica la fecha de creación del recurso';
COMMENT ON COLUMN resources.updated IS 'Identifica la fecha de actualización del recurso';