CREATE TABLE roles(
    id SERIAL NOT NULL,
    rolename VARCHAR(50) NOT NULL,
    roledesc VARCHAR(250) DEFAULT NULL,
    created TIMESTAMP DEFAULT now() NOT NULL,
    updated TIMESTAMP DEFAULT now() NOT NULL,
    CONSTRAINT roles_id_pk PRIMARY KEY (id),
    CONSTRAINT roles_rolename_uk UNIQUE (rolename)
);

COMMENT ON TABLE roles IS 'Es la entidad de Roles del sistema';

COMMENT ON COLUMN roles.id IS 'Clave principal de la entidad Roles';
COMMENT ON COLUMN roles.rolename IS 'Nombre del role';
COMMENT ON COLUMN roles.roledesc IS 'Descripción del role';
COMMENT ON COLUMN roles.created IS 'Identifica la fecha de creación del role';
COMMENT ON COLUMN roles.updated IS 'Identifica la fecha de actualización del role';