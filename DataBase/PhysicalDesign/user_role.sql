CREATE TABLE user_role(
    id SERIAL NOT NULL,
    user_id INTEGER NOT NULL,
    role_id INTEGER NOT NULL,
    created TIMESTAMP DEFAULT now() NOT NULL,
    updated TIMESTAMP DEFAULT now() NOT NULL,
    CONSTRAINT user_role_id_pk PRIMARY KEY (id),
    CONSTRAINT user_role_user_id_fk FOREIGN KEY (user_id)
        REFERENCES users(id),
    CONSTRAINT user_role_role_id_fk FOREIGN KEY (role_id)
        REFERENCES roles(id),
    CONSTRAINT user_role_user_id_role_id_uk UNIQUE (user_id, role_id)
);

COMMENT ON TABLE user_role IS 'Tabla pivote entre Users y Roles con relación de muchos a muchos';

COMMENT ON COLUMN user_role.id IS 'Clave principal de la entidad User_Role';
COMMENT ON COLUMN user_role.user_id IS 'Id del usuario (Users)';
COMMENT ON COLUMN user_role.role_id IS 'Id del role (Roles)';
COMMENT ON COLUMN user_role.created IS 'Identifica la fecha de cuando se le asigno un role a un usuario';
COMMENT ON COLUMN user_role.updated IS 'Identifica la fecha de actualización del role de un usuario';